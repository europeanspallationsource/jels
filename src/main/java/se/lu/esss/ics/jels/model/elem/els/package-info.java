/**
* 	Modeling elements ported from ELS implementation. They are in the reference frame TraceWin uses, 
*	(which is not native with OpenXal).
*
*	@author Emanuele Laface
*/

package se.lu.esss.ics.jels.model.elem.els;