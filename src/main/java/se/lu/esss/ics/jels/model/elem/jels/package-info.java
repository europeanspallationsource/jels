/**
* 	Combination of modeling elements from TraceWin documentation put into OpenXAL reference frame.
*   Thoroughly tested and compared to TraceWin results.
*/

package se.lu.esss.ics.jels.model.elem.jels;